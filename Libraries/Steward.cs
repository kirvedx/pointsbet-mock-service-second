using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace pointsbet_mock_service_second.Controllers
{
    public class Steward
    {
        /// <summary>
        /// The PointsBetMockServiceFirst Endpoint Location (Eventually we'll
        /// want to build a service registry to track all our endpoints.
        /// </summary>
        private string serviceFirst = "http://localhost:3000";

        /// <summary>Exposes the /users Endpoint of the PointsBetMockServiceFirst service.</summary>
        private string userEndpoint = "/users";

        /// <summary>Exposes the /bets Endpoint of the PointsBetMockServiceFirst service.</summary>
        private string betEndpoint = "/bets";

        public Steward()
        {
        }

        /// <summary>
        /// Guides the transaction of placing a bet using the service-first service API for managing resources.
        /// </summary>
        /// <param name="order">The {PlaceBetOrder} object containing the details required for placing a bet (betId, userId, stake).</param>
        /// <returns>A {PlaceBetDetails} object which contains the details pertaining to the result of the transaction.</returns>
        public async Task<PlaceBetDetails> ProcessRequest(PlaceBetOrder order)
        {
            // Get a transaction object pre-populated with a transactionId
            // that represents this bet placement transaction:
            PlaceBetDetails transaction = this.StartTransaction(order);

            // Attempt to get the user's current balance:
            transaction = await this.GetStartingBalance(order, transaction);

            // Here we can apply logic if, for instance, the user service was
            // down:
            if(transaction.status != null)
            {
                // Currently the default set-up is to return an error to the
                // caller that states the user service was unavailable:
                this.LogTransaction(transaction.transactionId, transaction);

                return transaction;
            }

            // Next we must determine the presumptuous balance, and even furthermore
            // discover if the bet placement request is for a new bet, or an update -
            // and if an update; whether the user is authorized to make such an update:
            transaction = await this.GetPresumptuousBalance(order, transaction);

            // Various cases exist with which an error can occur - and even one where
            // no update can be made as the provided data is what currently exists in
            // our system:
            if(transaction.status != null)
            {
                // Again our default set-up is to return an error to the
                // caller that states the error which occurred:
                this.LogTransaction(transaction.transactionId, transaction);

                return transaction;
            }

            // Next we create or update the bet record as per the request demands:
            transaction = await this.CommitBetRecordData(order, transaction);

            // If the bet could not be committed (created/updated):
            if(transaction.status != null)
            {
                // As we have been, we'll return an error to the caller
                // that states the error which occurred:
                this.LogTransaction(transaction.transactionId, transaction);

                return transaction;
            }

            // Next we update the user's balance appropriately:
            transaction = await this.CommitUserBalanceData(order, transaction);

            // If the balance could not be updated:
            if(transaction.status != null)
            {
                // We'll store an error that will state that the balance
                // could not be updated, but at least attempt to roll-back
                // the created bet (Either to its previous state, or by
                // zero-ing it out, as there's no delete functionality)
                transaction = await this.RollbackBetRecordData(order, transaction);

                this.LogTransaction(transaction.transactionId, transaction);

                return transaction;
            }

            // Finally, prepare the final details for the caller:
            transaction.endingBalance = transaction.presumptuousBalance;
            transaction.status = "SUCCESS";
            transaction.statusCode = "OK";
            transaction.message = (transaction.isUpdate) ? "The user's bet was updated successfully" : "The user's bet was placed successfully.";

            // Even log the completed transactions!
            this.LogTransaction(transaction.transactionId, transaction);

            // And return the transaction:
            return transaction;
        }

        /// <summary>
        /// Creates a transaction Id and pre-populates some transactional data
        /// </summary>
        /// <param name="order">The {PlaceBetOrder} object containing the order details provided in the request.</param>
        /// <returns>A {PlaceBetDetails} object which represents a bet placement transaction.</returns>
        public PlaceBetDetails StartTransaction(PlaceBetOrder order)
        {
            // Generate a transaction id for the bet placement, this will let us
            // track the transaction, and apply a worker in later development:
            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            // Return the transaction container with prepopulated metadata:
            return new PlaceBetDetails()
            {
                transactionId   = unixTimestamp.ToString(),
                isUpdate        = false,
                betId           = order.betId,
                userId          = order.userId,
                stake           = order.stake
            };
        }

        /// <summary>
        /// Gets the current balance of a user from the user service, and sets any flags if an error occurs.
        /// </summary>
        /// <param name="order">The {PlaceBetOrder} object which contains the details required for the request.</param>
        /// <param name="transaction">The {PlaceBetDetails} object which represents the bet placement transaction.</param>
        /// <returns>The {PlaceBetDetails} transaction object, updated with details from this step of the transaction.</returns>
        public async Task<PlaceBetDetails> GetStartingBalance(PlaceBetOrder order, PlaceBetDetails transaction)
        {
            // Make a resource call to get details on a user's balance:
            HttpResponseMessage balanceOrderResponse = await this.GetUserBalance(new BalanceOrder(){ userId = order.userId});

            // If the request for the user's balance fails:
            if(!balanceOrderResponse.IsSuccessStatusCode)
            {
                // Return to the caller that the balance could not be retrieved. We could
                // also subsequently try again, to a timeout of sorts, or even create a queue
                // where a 'worker' attempts at some frequency to finish transactions that have
                // failed for certain reasons (network failure, services lost, etc) while active.
                transaction.startingBalance = -1;
                transaction.endingBalance = transaction.startingBalance;
                transaction.status = "ERROR";
                transaction.statusCode = balanceOrderResponse.StatusCode.ToString();
                transaction.message = "The user balance could not be retrieved.";

                return transaction;
            }

            // Otherwise let's record the starting balance
            transaction.startingBalance = (await this.GetJSONFromResponse<Balance>(balanceOrderResponse)).balance;

            // And return the transaction in order to continue processing the request:
            return transaction;
        }

        /// <summary>
        /// Determines if the Bet Placement request is for a new or existing  bet, and subsequently
        /// calculates the presumptuous balance - raising any flags required for any errors discerned
        /// through out the process.
        /// </summary>
        /// <param name="order">The {PlaceBetOrder} object which contains the details required for the request.</param>
        /// <param name="transaction">The {PlaceBetDetails} object which represents the bet placement transaction.</param>
        /// <returns>The {PlaceBetDetails} transaction object, updated with details from this step of the transaction.</returns>
        public async Task<PlaceBetDetails> GetPresumptuousBalance(PlaceBetOrder order, PlaceBetDetails transaction)
        {
            // Before we can 'for certain' say that the user cannot afford the bet, we must
            // first determine if we are creating a new bet, or are possibly updating a
            // previously existing bet:
            HttpResponseMessage updateAnExistingBetResponse = await this.GetBetDetails(new BetDetailsOrder(){ betId = order.betId });

            if(updateAnExistingBetResponse.IsSuccessStatusCode)
            {
                transaction.isUpdate = true;
            }
            else
            {
                // If the response was "NotFound", well, we expected that. So we
                // won't return an error unless it's of a different nature:
                if(updateAnExistingBetResponse.StatusCode.ToString() != "NotFound")
                {
                    transaction.endingBalance = transaction.startingBalance;
                    transaction.status = "ERROR";
                    transaction.statusCode = updateAnExistingBetResponse.StatusCode.ToString();
                    transaction.message = "Could not check if the supplied betId already exists.";

                    return transaction;
                }
            }

            // We could subsequently have to be concerned with there being a different user Id associated
            // with this bet (literally, there're no guards in place to enforce _anything_) - so we'll show
            // the logic behind determining how to adjust the balance, after ensuring the request was made
            // by the appropriate owner of the bet:
            if(transaction.isUpdate)
            {
                // Parse the prior bet's details into an object we can access them from:
                BetDetails priorBet = await this.GetJSONFromResponse<BetDetails>(updateAnExistingBetResponse);

                if(priorBet.userId != order.userId)
                {
                    // Bad request, the calling gambler is not the owner of the bet:
                    transaction.endingBalance = transaction.startingBalance;
                    transaction.status = "ERROR";
                    transaction.statusCode = "Unauthorized";
                    transaction.message = "The userId specified by the request does not own the resource requested to be updated.";

                    return transaction;
                }

                // Set the previous stake amount in case of a roll-back:
                transaction.previousStake = priorBet.stake;
            }

            // Figure out the presumptousBalance:
            transaction.presumptuousBalance = transaction.startingBalance - order.stake;

            transaction.resultingStake = transaction.previousStake + order.stake;

            // If somehow we ended up with a stake of <= 0, we should not bother updating the user
            // balance or even with adding a bet:
            if(transaction.presumptuousBalance >= transaction.startingBalance)
            {
                transaction.endingBalance = transaction.startingBalance;
                transaction.resultingStake = transaction.previousStake;
                transaction.status = "ERROR";
                transaction.statusCode = "BadRequest";
                transaction.message = "The user must pass a stake greater than 0.";

                return transaction;
            }

            // If the users balance does not allow for the bet (creation/update) we must return an error:
            //if(!(transaction.startingBalance >= order.stake) || (transaction.isUpdate && transaction.presumptuousBalance < 0))
            if(transaction.presumptuousBalance < 0)
            {
                transaction.endingBalance = transaction.startingBalance;
                transaction.resultingStake = transaction.previousStake;
                transaction.status = "ERROR";
                transaction.statusCode = "InsufficientFunds";
                transaction.message = "The user's balance does not allow for the supplied bet amount.";

                return transaction;
            }

            return transaction;
        }

        /// <summary>
        /// Makes a request to the Bet service in an attempt to commit bet record details. Sets flags if an error occurs.
        /// </summary>
        /// <param name="order">The {PlaceBetOrder} object which contains the details required for the request.</param>
        /// <param name="transaction">The {PlaceBetDetails} object which represents the bet placement transaction.</param>
        /// <returns>The {PlaceBetDetails} transaction object, updated with details from this step of the transaction.</returns>
        public async Task<PlaceBetDetails> CommitBetRecordData(PlaceBetOrder order, PlaceBetDetails transaction)
        {
            // Ensure we set the appropriate stake if this was an update:
            HttpResponseMessage betCreated = await this.CreateBet(new BetOrder(){ betId = order.betId, userId = order.userId, stake = (transaction.isUpdate)? transaction.resultingStake : order.stake });

            // If the bet could not be created/updated then we return an error
            if(!betCreated.IsSuccessStatusCode)
            {
                transaction.endingBalance = transaction.startingBalance;
                transaction.resultingStake = transaction.previousStake;
                transaction.status = "ERROR";
                transaction.statusCode = betCreated.StatusCode.ToString();
                transaction.message = "The bet could not be created.";

                //return transaction;
            }

            return transaction;
        }

        /// <summary>
        /// Rolls back the created/updated bet to its previous state before the transaction modified it.
        /// </summary>
        /// <param name="order">The {PlaceBetOrder} object which contains the details required for the request.</param>
        /// <param name="transaction">The {PlaceBetDetails} object which represents the bet placement transaction.</param>
        /// <returns>The {PlaceBetDetails} transaction object, updated with details from this step of the transaction.</returns>
        public async Task<PlaceBetDetails> RollbackBetRecordData(PlaceBetOrder order, PlaceBetDetails transaction)
        {
            // As we were unable to update the user's balance we will attempt to
            // roll-back the bet record which was updated/created:
            HttpResponseMessage betRolledBack = await this.CreateBet(new BetOrder(){ betId = order.betId, userId = order.userId, stake = transaction.previousStake });

            // If the bet could not be rolled-back we update the error
            if(!betRolledBack.IsSuccessStatusCode)
            {
                transaction.endingBalance = transaction.startingBalance;
                transaction.status = "ERROR";
                transaction.statusCode = betRolledBack.StatusCode.ToString();

                // If we subsequently are unable to roll-back the bet, we'll identify
                // the failureState as 1: Failed, and unable to roll-back the bet.
                transaction.failureState = 2;
                transaction.message = "[Failed] " + transaction.message + " Unable to roll-back the bet.";

                return transaction;
            }

            // Assuming that we were able to roll-back the bet, we'll identify
            // the failureState as 1: Failed, but rolled-back.
            transaction.resultingStake = transaction.previousStake;
            transaction.failureState = 1;
            transaction.message = "[Failed] " + transaction.message + " The bet was rolled-back.";

            return transaction;
        }

        /// <summary>
        /// Makes a request to the User service in an attempt to commit user balance details. Sets flags if an error occurs.
        /// </summary>
        /// <param name="order">The {PlaceBetOrder} object which contains the details required for the request.</param>
        /// <param name="transaction">The {PlaceBetDetails} object which represents the bet placement transaction.</param>
        /// <returns>The {PlaceBetDetails} transaction object, updated with details from this step of the transaction.</returns>
        public async Task<PlaceBetDetails> CommitUserBalanceData(PlaceBetOrder order, PlaceBetDetails transaction)
        {
            HttpResponseMessage balanceUpdated = await this.UpdateUserBalance(new BalanceUpdateOrder(){ userId = order.userId, balance = transaction.presumptuousBalance });

            // If the balance could not be  updated then we return an error:
            if( !balanceUpdated.IsSuccessStatusCode)
            {
                transaction.endingBalance = transaction.startingBalance;
                transaction.status = "ERROR";
                transaction.statusCode = balanceUpdated.StatusCode.ToString();
                transaction.message = "The user's balance could not be updated.";

                //return transaction;
            }

            return transaction;
        }

        /// <summary>
        /// Deserializes a JSON string from an HttpResponseMessage into a specified object.
        /// </summary>
        /// <param name="response">The {HttpResponseMessage} containing the JSON string to be deserialized.</param>
        /// <typeparam name="T">The type of object the JSON string is required to be deserialized to.</typeparam>
        /// <returns>The object type that the JSON string is requested to be desierialized as.</returns>
        public async Task<T> GetJSONFromResponse<T>(HttpResponseMessage response)
        {
            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// Serializes an object into a JSON string that can be converted into a StringContent for HTTP POST requests.
        /// </summary>
        /// <param name="details">The {T} details object being serialized for the HTTP POST request.</param>
        /// <typeparam name="T">The type of object being serialzed.</typeparam>
        /// <returns>The JSON string that can be converted into a StringContent for HTTP POST requests.</returns>
        public string GetJSONForResponse<T>(T details)
        {
            return JsonConvert.SerializeObject(details);
        }

        /// <summary>
        /// Guides a request for fetching a user's current balance.
        /// </summary>
        /// <param name="order">A {BalanceOrder} object containing the required details for requesting a user's balance (userId).</param>
        /// <returns>A {HttpResponseMessage} object which is the result of the request.</returns>
        public async Task<HttpResponseMessage> GetUserBalance(BalanceOrder order)
        {
            HttpResponseMessage response = await this.CallService(this.serviceFirst, this.userEndpoint + "/" + order.userId);

            return response;
        }

        /// <summary>
        /// Guides a request for updating a user's balance.
        /// </summary>
        /// <param name="order">A {BalanceUpdateOrder} object containing the required details for updating a user's balance (userId, balance).</param>
        /// <returns>A {HttpResponseMessage} object which is the result of the request.</returns>
        public async Task<HttpResponseMessage> UpdateUserBalance(BalanceUpdateOrder order)
        {
            Balance details = new Balance(){ balance = order.balance };

            HttpResponseMessage response = await this.CallService(this.serviceFirst, this.userEndpoint + "/" + order.userId, this.GetJSONForResponse(details), "POST");

            return response;
        }

        /// <summary>
        /// Guides a request for fetching details about a specific bet.
        /// </summary>
        /// <param name="order">A {BetDetailsOrderObject} with contains the required details for requesting details on a bet (betId).</param>
        /// <returns>A {HttpResponseMessage} object which is the result of the request.</returns>
        public async Task<HttpResponseMessage> GetBetDetails(BetDetailsOrder order)
        {
            HttpResponseMessage response = await this.CallService(this.serviceFirst, this.betEndpoint + "/" + order.betId);

            return response;
        }

        /// <summary>
        /// Guides a request for creating/updating a bet record.
        /// </summary>
        /// <param name="order">A {BetOrder} object containing the required details for creating/updating a bet (betId, userId, stake).</param>
        /// <returns>A {HttpResponseMessage} object which is the result of the request.</returns>
        public async Task<HttpResponseMessage> CreateBet(BetOrder order)
        {
            BetDetails details = new BetDetails(){ userId = order.userId, stake = order.stake };

            HttpResponseMessage response = await this.CallService(this.serviceFirst, this.betEndpoint + "/" + order.betId, this.GetJSONForResponse(details), "POST");

            return response;
        }

        /// <summary>
        /// Makes a request to a web service using the provided data.
        /// </summary>
        /// <param name="url">The {string} url of the service a request is being made to.</param>
        /// <param name="query">The {string} request URI.</param>
        /// <param name="content">A {string} that will be used for the request body (POST only, Optional, default is empty.).</param>
        /// <param name="method">A {string} that specifies the HTTP method, either 'GET'or 'POST' (Optional, default is 'GET').</param>
        /// <param name="contentType">A {string} that specifies the 'Content-Type' header of the request (Optional, default is 'application/json').</param>
        /// <returns>A {HttpResponseMessage} object which is the result of the request.</returns>
        public async Task<HttpResponseMessage> CallService(string url, string query, string content = "", string method = "GET", string contentType = "application/json")
        {
            // Use HttpClient to do the magic:
            HttpClient client = new HttpClient();

            // We need to access the serviceFirst microservice:
            client.BaseAddress = new Uri(url);

            // Add an Accept header for JSON format:
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));

            if( method == "GET" )
            {
                // Make the request (which right now is just asking for the user's balance)
                return await client.GetAsync(query);
            }

            return await client.PostAsync(query, new StringContent(content, Encoding.UTF8, contentType));
        }

        /// <summary>
        /// Loads a log from a json file
        /// </summary>
        /// <returns>Returns a list from the task. </returns>
        public async Task<SortedDictionary<string, PlaceBetDetails>> LoadLog()
        {
            // Read the transaction log in:
            string transactionLogData = await System.IO.File.ReadAllTextAsync("./store/transaction_log.json");

            // Return the deserialized object:
            return JsonConvert.DeserializeObject<SortedDictionary<string, PlaceBetDetails>>(transactionLogData);
        }

        /// <summary>
        /// Saves a log in a [json] file
        /// </summary>
        /// <param name="log">The list to be saved</param>
        /// <returns>void</returns>
        public async void SaveLog(SortedDictionary<string, PlaceBetDetails> log)
        {
            // Convert the log into a [json] string:
            string updatedLogData = JsonConvert.SerializeObject(log, Formatting.Indented);

            // Save the [json] string to disk:
            await System.IO.File.WriteAllTextAsync("./store/transaction_log.json", updatedLogData);
        }

        /// <summary>
        /// Logs a completed transaction for rollback and audit purposes
        /// </summary>
        /// <param name="key">A {string} reflecting the key identifier for a completed transaction.</param>
        /// <param name="transaction">An {HttpResponseMessage} reflecting a completed transaction.</param>
        /// <returns>void</returns>
        public async void LogTransaction(string key, PlaceBetDetails transaction)
        {
            // Read the transaction log in:
            SortedDictionary<string, PlaceBetDetails> transactionLog = await this.LoadLog();

            // Add the new transaction into the log:
            transactionLog.Add(key, transaction);

            // Write the transaction log out:
            this.SaveLog(transactionLog);
        }
    }
}