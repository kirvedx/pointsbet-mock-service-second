namespace pointsbet_mock_service_second.Controllers
{
    public class PlaceBetOrder
    {
        public string betId { get; set; }
        public string userId { get; set; }
        public float stake { get; set; }
    }
}