namespace pointsbet_mock_service_second.Controllers
{
    public class BetDetails
    {
        public string userId { get; set; }
        public float stake { get; set; }
    }
}