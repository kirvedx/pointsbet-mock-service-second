namespace pointsbet_mock_service_second.Controllers
{
    public class PlaceBetDetails
    {
        public string status { set; get; }
        public string statusCode { set; get; }
        public string transactionId { set; get; }
        public bool isUpdate { set; get; }
        public string betId { set; get; }
        public string userId { set; get; }
        public float startingBalance { set; get; }
        public float presumptuousBalance { set; get; }
        public float endingBalance { set; get; }
        public float stake { set; get; }
        public float resultingStake { set; get; }
        public float previousStake { set; get; }
        public int failureState{ set; get; }
        public string message { set; get; }
    }
}