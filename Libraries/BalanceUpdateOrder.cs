namespace pointsbet_mock_service_second.Controllers
{
    public class BalanceUpdateOrder
    {
        public string userId { get; set; }
        public float balance { get; set; }
    }
}