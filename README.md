# pointsbet-mock-service-second

[![pipeline status](https://gitlab.com/devrikx/pointsbet-mock-service-second/badges/master/pipeline.svg)](https://gitlab.com/devrikx/pointsbet-mock-service-second/commits/master)  [![coverage report](https://gitlab.com/devrikx/pointsbet-mock-service-second/badges/master/coverage.svg)](https://devrikx.gitlab.io/pointsbet-mock-service-second/coverage/)

The C# mock API requested for this challenge that will be served as *service second*. It will function as a bet placement API which will interface with an Express-based Node application (*service first*) provided by PointsBeT.

## TOC
* [Implementation Details](#implementation-details)
* [Build Instructions](#build-instructions)
  * [Git the Source](#git-the-source)
  * [Install Dependencies](#install-dependencies)
  * [Build the Application](#build-the-application)
  * [Test the application](#test-the-application)
* [Other Considerations](#other-considerations)
  * [Gitflow](#gitflow)
  * [Continuous Integration and Deployment](#continuous-integration-and-deployment)
  * [PointsBet Challenge](#pointsbet-challenge)
* [Tools](#tools)

## Implementation Details

The application shell consists of the following:

* DotNet Core 2.2 for the application platform.
* Nuget for project dependency management.
* A C# based WebAPI project template.

## Build Instructions

The following steps should be taken in order to build this application:

### Git the Source

Clone the repository from [Gitlab](https://gitlab.com/devrikx/pointsbet-mock-service-second):

*via HTTPS*:

```bash
git clone https://gitlab.com/devrikx/pointsbet-mock-service-second
```

*via Git+SSH*:

```bash
git clone git@gitlab.com:devrikx/pointsbet-mock-service-second
```

### Install dependencies

The following dependencies are required:

* [pointsbet-mock-service-first](https://gitlab.com/devrikx/pointsbet-mock-service-first) needs to be built and run on any environment that this project is expected to be run in, as it is consumed by the service this project implements.

#### Linux

* [.NET Core 2.2 (Debian Buster/Unstable Users README)](https://github.com/dotnet/cli/issues/10103#issuecomment-449229155), for anyone else start [here](https://www.microsoft.com/net/download/linux-package-manager/debian9/sdk-current) and change the distro that you're installing for.
* [Visual Studio Code](https://code.visualstudio.com/)
  * C# Extension ( via Omnisharp - It's a Microsoft extension, the first in the list)
  * Upon opening the project in Visual Studio Code for the first time, you'll be prompted to download and install all project dependencies via NuGet (An alert will pop-up in the bottom right, be sure to select yes!).
  * After the dependencies are installed, you'll need some additional tools. Open the terminal `CTRL + ~`, `cd` to the project root, and run the following commands:
    * `dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design --version 2.2.0`
    * `dotnet tool install --global dotnet-aspnet-codegenerator` (Generating the default values controller of a new WebAPI project becomes as simple as: `dotnet aspnet-codegenerator controller -name Values -api -actions -outDir Controllers -f`)

#### Windows

* .NET Core 2.2
* Visual Studio *or* Visual Studio Code
  * Similarly to Linux you will need to open the project for the first time for project dependencies to then be downloaded and installed (for both Visual Studio & Visual Studio Code).
  * If Visual Studio Code is installed, you'll need the C# Extension.
  * If Visual Studio Code is installed, you'll also want to install the same additional packages as for Linux (Commands are the same).

### Build the Application

Building the application may differ depending on the environment the project is being run in:

#### Linux & Mac

Now that you have the source and all the dependencies installed, run the following command to build the application:

```bash
dotnet restore
dotnet build
```

#### Windows

Use the Visual Studio IDE to build the WebAPI application.

### Test the Application

There are two different ways you can test the application:

#### Development

To test the application as a developer, you may leverage the tests which are provided with the source. The tests are built using [?](#)

Run the following command:

```bash
dotnet test
```

#### Production

To run the application for testing in a staging or production environment, run the following command:

```bash
dotnet run
```

You can then visit `http://localhost:5000/api/<endpoint>` or `https://localhost:5001/api/<endpoint>` in a browser - or using a tool like [Postman](https://www.getpostman.com/) to test the endpoints of the service.

## Other Considerations

As a compliment to the code itself, I've made use of some common features of Gitlab for performing common project management tasks, as well as in making use of best practices.

To see what I've done, explore the Project Management features of Gitlab in the following ways:

### Gitflow

* The [project board](https://gitlab.com/devrikx/pointsbet-mock-service-second/boards?=) can be visited to explore [user-scenarios/tasks](https://gitlab.com/devrikx/pointsbet-mock-service-second/issues), and [milestones](https://gitlab.com/devrikx/pointsbet-mock-service-second/milestones) which helped to guide completion of the challenge.
  * [Issue-First Development](https://docs.gitlab.com/ee/workflow/gitlab_flow.html) was used to direct the management for later bits of the challenge. View the graph [HERE](https://gitlab.com/devrikx/pointsbet-mock-service-second/network/master)

### Continuous Integration and Deployment

I leveraged the job/pipeline features of Gitlab in the following ways:

* When checking in the master branch, a [pipline](https://gitlab.com/devrikx/pointsbet-mock-service-second/pipelines) is utilized in testing, building, and deploying the challenge application.
  * [Jobs](https://gitlab.com/devrikx/pointsbet-mock-service-second/-/jobs) are dispatched with every commit to master; Test and Deploy. [Here](https://gitlab.com/devrikx/pointsbet-mock-service-second/blob/master/.gitlab-ci.yml)'s the configuration.

### PointsBet Challenge

You can review the [PointsBet Service](https://devrikx.gitlab.io/pointsbet-mock-service-second/), continuously deployed to a kubernetes instance on my local cluster upon a check-in to master. The service is baked into a container and run as a pod, side-by-side with the [pointsbet-mock-service-first](https://gitlab.com/devrikx/pointsbet-mock-service-first) service that is also baked into its own container and run as a pod.

## Tools

The tools leveraged in building this template include:

* [Visual Studio Code](https://code.visualstudio.com/)
