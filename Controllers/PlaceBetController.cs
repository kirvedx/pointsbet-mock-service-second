using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace pointsbet_mock_service_second.Controllers
{
    [Produces("application/json")]
    [Route("api/place-bet")]
    [ApiController]
    public class PlaceBetController : Controller
    {
        /// <summary>
        /// The Steward class implements custom transaction processes.
        /// </summary>
        public Steward steward = new Steward();


        // GET api/place-bet/1/5
        [HttpGet]
        public ActionResult<string> Get()
        {
            return "A route for placing bets has been accessed by GET! ";
        }

        // POST api/place-bet
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] PlaceBetOrder bet)
        {
            // Hand the request off to our steward to handle it for us:
            PlaceBetDetails transaction = await this.steward.ProcessRequest(bet);

            // Because we are implementing a transaction, let us handle cases
            // of potential error along the resource chain, (not inclusive):
            switch(transaction.statusCode)
            {
                // Treat 'Unauthorized' and 'InsufficientFunds' as a 'BadRequest':
                case "Unauthorized":
                case "InsufficientFunds":
                case "BadRequest":
                {
                    return BadRequest(transaction);
                };

                case "NotFound":
                {
                    return NotFound(transaction);
                };

                default:
                {
                    return Ok(transaction);
                }
            }
        }

        // PUT api/place-bet/1
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] PlaceBetOrder updatedBet)
        {
        }

        // DELETE api/place-bet/1
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
